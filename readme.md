# Markup Engine
An engine that created for easily access datas and entries using boost like string path

## Supported Markup Language
- XML
- JSON
- YAML

## Require libraries
- [pugixml (Header only mode)](http://pugixml.org)
- [RapidJSON](http://rapidjson.org)
- [yaml-cpp](https://github.com/jbeder/yaml-cpp)