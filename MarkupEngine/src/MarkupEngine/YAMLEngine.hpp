#pragma once
#include <string>
#include <yaml-cpp/yaml.h>
#include <vector>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

namespace mkengine
{
	template <typename StringType = std::string>
	class YAMLEngineTemplate
	{
	private:
		YAML::Node document;
	public:
		YAMLEngineTemplate() = default;
		YAMLEngineTemplate(const YAMLEngineTemplate&) = default;
		YAMLEngineTemplate(YAMLEngineTemplate&&) = default;
		YAMLEngineTemplate(const YAML::Node& rhs)
		{
			document = rhs;
		}
		YAMLEngineTemplate(YAML::Node&& rhs)
		{
			document = std::move(rhs);
		}
		~YAMLEngineTemplate() = default;

		void loadFile(const StringType& name)
		{
			document = YAML::LoadFile(name);
		}

		void loadFile(std::ifstream& inputFile)
		{
			document = YAML::Load(inputFile);
		}

		void loadString(const StringType& data)
		{
			document = YAML::Load(data);
		}

		StringType dump() const
		{
			return YAML::Dump(document);
		}

		void dump(std::ofstream & outputFile) const
		{
			outputFile << dump();
		}

		void dump(const StringType & filePath) const
		{
			std::ofstream output;
			output.exceptions(std::ofstream::failbit | std::ofstream::badbit);
			output.open(filePath);
			dump(output);
			output.close();
		}

		YAML::Node getNode()
		{
			return this->document;
		}

		YAML::Node getNode(const StringType & path)
		{
			if (path.empty()) return document;

			std::vector<StringType> pathHorizontal;
			boost::algorithm::split(pathHorizontal, path, [](char t) { return t == '.'; });
			if (pathHorizontal.empty()) return document;

			YAML::iterator documentItr = document.begin();
			YAML::iterator documentItrLim = document.end();
			YAML::iterator documentItrStart = document.begin();
			bool sequenceMode = document.IsSequence();
			size_t pathIndex = 0;

			while (documentItr != documentItrLim && pathIndex < pathHorizontal.size())
			{
				if (sequenceMode)
				{
					if (std::distance(documentItrStart, documentItr) == boost::lexical_cast<size_t>(pathHorizontal[pathIndex]))
					{
						pathIndex++;
						if (pathIndex < pathHorizontal.size())
						{
							sequenceMode = documentItr->second.IsSequence();
							documentItrLim = documentItr->second.end();
							documentItr = documentItr->second.begin();
							documentItrStart = documentItr;
						}
					}
					else
						documentItr++;
				}
				else if (documentItr->first.as<StringType>() == pathHorizontal[pathIndex])
				{
					pathIndex++;
					if (pathIndex < pathHorizontal.size())
					{
						sequenceMode = documentItr->second.IsSequence();
						documentItrLim = documentItr->second.end();
						documentItr = documentItr->second.begin();
						documentItrStart = documentItr;
					}
				}
				else
					documentItr++;
			}

			if (documentItr == documentItrLim)
				return YAML::Node(YAML::NodeType::Undefined);
			else
				return (sequenceMode) ? YAML::Node(*documentItr) : documentItr->second;
		}

		const YAML::Node getNode() const
		{
			return document;
		}

		const YAML::Node getNode(const StringType & path) const
		{
			return const_cast<YAMLEngineTemplate*>(this)->getNode(path);
		}

		operator YAML::Node&()
		{
			return document;
		}

		operator const YAML::Node&() const
		{
			return document;
		}

		YAMLEngineTemplate& operator=(const YAMLEngineTemplate & rhs)
		{
			document = rhs.document;
			return *this;
		}

		YAMLEngineTemplate& operator=(YAMLEngineTemplate && rhs)
		{
			document = std::move(rhs.document);
			return *this;
		}

		YAMLEngineTemplate& operator=(const YAML::Node & rhs)
		{
			document = rhs;
			return *this;
		}

		YAMLEngineTemplate& operator=(YAML::Node && rhs)
		{
			document = std::move(rhs);
			return *this;
		}

		template<typename Key, typename Value>
		void insert(const StringType & parentPath, Key key, Value value)
		{
			this->getNode(parentPath).force_insert(key, value);
		}

		template<typename Key>
		void insert(const StringType & parentPath, Key key)
		{
			this->getNode(parentPath).force_insert(key, YAML::Node(YAML::NodeType::Null));
		}

		template<typename OutputType>
		OutputType getEntry(const StringType & path)
		{
			auto result = this->getNode(path);
			if (result.IsDefined() && !result.IsNull())
				return result.as<OutputType>();
			else
			{
				throw std::runtime_error("cannot find node \"" + path + "\".");
				return OutputType();
			}
		}
	};
	using YAMLEngine = YAMLEngineTemplate<>;
}