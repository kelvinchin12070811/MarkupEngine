#pragma once
#include <memory>
#include <rapidjson/document.h>
#include <rapidjson/pointer.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/prettywriter.h>

namespace mkengine
{
	template <typename StringType = std::string>
	class JSONEngineTemplate
	{
	private:
		rapidjson::Document document;
	private:
		const StringType stringPath2JsonPath(const StringType & stringPath) const
		{
			StringType jsonPath = "/";
			std::vector<StringType> horiPath;
			boost::algorithm::split(horiPath, stringPath, [](char t) {return t == '.'; });
			jsonPath += boost::algorithm::join(horiPath, "/");
			return jsonPath;
		}
	public:
		enum class PathType : uint8_t { string_path, json_pointer_path };
	public:
		JSONEngineTemplate()
		{
		}

		JSONEngineTemplate(JSONEngineTemplate && rhs) :
			document(std::move(rhs.document))
		{
		}

		~JSONEngineTemplate()
		{
		}

		void loadFile(const StringType & path)
		{
			std::ifstream inputFile;
			//inputFile.exceptions(std::ifstream::badbit | std::ifstream::failbit);
			inputFile.open(path);
			if (!inputFile.is_open()) throw std::runtime_error("cannot open file " + path);
			loadFile(inputFile);

			inputFile.close();
		}

		void loadFile(std::ifstream & inputFile)
		{
			rapidjson::IStreamWrapper wInputFile(inputFile);
			document.ParseStream(wInputFile);
		}

		void loadString(const StringType & buffer)
		{
			document.Parse(buffer.c_str());
		}

		StringType dump() const
		{
			std::stringstream ss;
			rapidjson::OStreamWrapper oss(ss);
			rapidjson::PrettyWriter<rapidjson::OStreamWrapper> pWriter(oss);
			document.Accept(pWriter);
			return ss.str();
		}

		void dump(std::ofstream & output) const
		{
			output << dump();
		}

		void dump(const StringType & file) const
		{
			std::ofstream output;
			output.exceptions(std::ofstream::failbit | std::ofstream::badbit);
			output.open(file);
			dump(output);
			output.close();
		}

		rapidjson::Pointer queryPointer(const StringType & path, PathType pathType = PathType::string_path)
		{
			StringType jsonPath = (pathType == PathType::json_pointer_path) ? path : stringPath2JsonPath(path);
			return rapidjson::Pointer(jsonPath.c_str());
		}

		const rapidjson::Pointer queryPointer(const StringType & path, PathType pathType = PathType::string_path) const
		{
			return const_cast<JSONEngineTemplate*>(this)->queryPointer(path, pathType);
		}

		rapidjson::Value * getNode(const StringType & path, rapidjson::Value* root = nullptr, PathType pathType = PathType::string_path)
		{
			auto result = this->queryPointer(path, pathType);
			if (root == nullptr)
				return result.Get(document);
			else
				return result.Get(*root);
		}

		const rapidjson::Value * getNode(const StringType & path, rapidjson::Value * root = nullptr, PathType pathType = PathType::string_path) const
		{
			return const_cast<JSONEngineTemplate*>(this)->getNode(path, root, pathType);
		}

		rapidjson::Value & getNode(const StringType & path, const rapidjson::Value & def, rapidjson::Value * root = nullptr, rapidjson::MemoryPoolAllocator<>* allocator = nullptr, PathType pathType = PathType::string_path)
		{
			auto result = this->queryPointer(path, pathType);
			if (root)
			{
				if (!allocator) throw std::runtime_error("allocator cannot be nullptr");
				return result.GetWithDefault(*root, def, *allocator);
			}
			else
				return result.GetWithDefault(document, def);
		}

		const rapidjson::Value & getNode(const StringType & path, const rapidjson::Value & def, rapidjson::Value * root = nullptr, rapidjson::MemoryPoolAllocator<>* allocator = nullptr, PathType pathType = PathType::string_path) const
		{
			return const_cast<JSONEngineTemplate*>(this)->getNode(path, def, root, allocator, pathType);
		}

		rapidjson::Value & create(const StringType & path, bool* alreadyExits = nullptr, rapidjson::Value * root = nullptr, rapidjson::MemoryPoolAllocator<>* allocator = nullptr, PathType pathType = PathType::string_path)
		{
			auto result = queryPointer(path, pathType);
			if (root)
			{
				if (!allocator) throw std::runtime_error("allocator cannot be nullptr");
				return result.Create(*root, *allocator, alreadyExits);
			}
			else
				return result.Create(document, alreadyExits);
		}

		rapidjson::Document * operator->()
		{
			return &document;
		}

		const rapidjson::Document * operator->() const
		{
			return &document;
		}

		rapidjson::Document & operator*()
		{
			return document;
		}

		const rapidjson::Document & operator*() const
		{
			return document;
		}

		JSONEngineTemplate & operator=(JSONEngineTemplate && rhs)
		{
			document = std::move(rhs.document);
			return *this;
		}

		operator rapidjson::Document&()
		{
			return document;
		}

		operator const rapidjson::Document&() const
		{
			return document;
		}
	};
	using JSONEngine = JSONEngineTemplate<>;
}